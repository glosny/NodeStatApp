const canvas = document.getElementById('chart')
const loader = document.getElementById('loader')
const ctx = canvas.getContext('2d');


let stockSymbols =
  fetch('http://localhost:3000/stocks/')
  .then(res => res.json())
  .then(function(data){
    let items = data;
    items = items.stockSymbols;

    Promise.all(items.map((sym) =>
      fetch('http://localhost:3000/stocks/' + sym)
      .then(res => res.json())
    ))
    .then(json => {
      items = [];

      json.forEach( item => {
        let imt = item.map(elm =>{
          return { y: elm.value , x: elm.timestamp }
        })
        items.push(imt)
      });

      let timeJson = json[0];
      timeOut = timeJson.map(item => {
        let time = new Date (item.timestamp )
        time = time.toLocaleTimeString()
        return time
      });

      const myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: timeOut,
                datasets: [{
                              label: 'Wartości dla MSFT',
                              data: items[0],
                              borderColor: 'rgba(255,99,132,1)',
                              backgroundColor: 'rgba(255,99,132,0.7)',
                              borderWidth: 1
                            },
                            {
                              label: 'Wartości dla AAPL',
                              data: items[1],
                              borderColor: 'rgba(010,88,111,1)',
                              backgroundColor: 'rgba(010,88,111,0.7)',
                              borderWidth: 1
                            },
                            {
                              label: 'Wartości dla FB',
                              data: items[2],
                              borderColor: 'rgba(025,155,002,1)',
                              backgroundColor: 'rgba(025,155,002,0.7)',
                              borderWidth: 1
                            },
                            {
                              label: 'Wartości dla EA',
                              data: items[3],
                              borderColor: 'rgba(206,158,022,1)',
                              backgroundColor: 'rgba(206,158,022,0.7)',
                              borderWidth: 1
                            },
                            {
                              label: 'Wartości dla IBM',
                              data: items[4],
                              borderColor: 'rgba(188,000,188,1)',
                              backgroundColor: 'rgba(188,000,188,0.7)',
                              borderWidth: 1
                            }
                          ]},
              options: {
                  scales: {
                      yAxes: [{
                        ticks: {
                            beginAtZero:true
                                }
                              }]
                          }
                        }
              });
    drawCHart();
    })
    .catch(err => console.log('Error!!'))
  });


function drawCHart() {
  loader.style.display = 'none';
  canvas.style.display = 'block';
}
